#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Train Residual U-Net')

help = ('The data directory; *image.nii(.gz) are the training images, '
        '*label.nii(.gz) are the training truth label images. If '
        '*mask.nii(.gz) exist, the images and label images will be cropped '
        'around *mask.nii(.gz)')
parser.add_argument('-i', '--input-dir', help=help, required=True)
parser.add_argument('-o', '--output-prefix', required=True,
                    help='Outut model prefix')
parser.add_argument('-d', '--depth', type=int, default=4,
                    help='The depth/number of transition down')
parser.add_argument('-w', '--width', type=int, default=16,
                    help='The width/number of channels of the first block')
parser.add_argument('-v', '--validation-indices', nargs='+', type=int,
                    help='The indicies of validation data in args.input_dir')
parser.add_argument('-c', '--cropping-shape', nargs='+', type=int,
                    help='The shape of cropped images (if *mask* exist)')
parser.add_argument('-b', '--batch-size', type=int, default=1,
                    help='The number of images per batch')
parser.add_argument('-e', '--num-epochs', type=int, default=200,
                    help='The number of epochs')
parser.add_argument('-p', '--saving-period', type=int, default=10,
                    help='Save the model every this number of epochs')
parser.add_argument('-l', '--learning-rate', type=float, default=0.001,
                    help='Adam learning rate')
parser.add_argument('-s', '--dropout-rate', type=float, default=0.5,
                    help='Dropout rate')

args = parser.parse_args()

import os
from glob import glob
import nibabel as nib
import numpy as np

import torch
from torch.optim import SGD, Adam
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

from network_utils import TrainingDataFactory as DF
from network_utils import Dataset3dFactory as DSF
from network_utils import LabelImageBinarizer

from pytorch_residual_unet.model import ResidualUNet
from pytorch_engine import Configurations as EngineConfigurations
from pytorch_engine.training import SimpleTrainer, Printer, ModelSaver
from pytorch_engine.training import SimpleValidator
from pytorch_engine.training.loggers import BasicLogger
from pytorch_engine.loss import DiceLoss


print('current device', torch.cuda.current_device())
print('count', torch.cuda.device_count())
print('name', torch.cuda.get_device_name(0))

image_paths = sorted(glob(os.path.join(args.input_dir, '*_image.*')))
label_paths = sorted(glob(os.path.join(args.input_dir, '*_label.*')))
mask_paths = sorted(glob(os.path.join(args.input_dir, '*_mask.*')))

num_classes = len(np.unique(nib.load(label_paths[0]).get_data()))

engine_configs = EngineConfigurations()
for i in range(num_classes):
    engine_configs.metrics.append('dice_%d' % i)
engine_configs.metrics.append('dice_1-%d' % (num_classes-1))

engine_configs.dropout_rate = args.dropout_rate

data_factory = DF()
binarizer = LabelImageBinarizer()
t_dataset, v_dataset = DSF.create(data_factory, args.validation_indices,
                                  image_paths, label_paths,
                                  mask_paths=mask_paths,
                                  cropping_shape=args.cropping_shape,
                                  binarizer=binarizer)

print('-' * 80)
print('TRAINING')
for d in t_dataset.data:
    print(*[dd.filepath for dd in d])
print('Number:', len(t_dataset))

print('-' * 80)
print('VALIDATION')
for d in v_dataset.data:
    print(*[dd.filepath for dd in d])
print('Number:', len(v_dataset))
print('-' * 80)

unet = ResidualUNet(1, num_classes, args.depth, args.width)
print(unet)

loss_func = DiceLoss()
optimizer = Adam(unet.parameters(), lr=args.learning_rate)
t_loader = DataLoader(t_dataset, batch_size=args.batch_size, shuffle=True)
v_loader = DataLoader(v_dataset, batch_size=1, shuffle=False)

num_batches = len(t_dataset) // args.batch_size \
            + ((len(t_dataset) % args.batch_size) > 0)
trainer = SimpleTrainer(unet, loss_func, optimizer,
                        num_epochs=args.num_epochs,
                        num_batches=num_batches,
                        data_loader=t_loader)
printer = Printer('training')
trainer.register_observer(printer)
saver = ModelSaver(args.saving_period, args.output_prefix+'{epoch}.pt')
trainer.register_observer(saver)
t_logger = BasicLogger(args.output_prefix + 'training.csv')
trainer.register_observer(t_logger)

validator = SimpleValidator(v_loader, num_batches=num_batches)
v_printer = Printer('validation')
v_logger = BasicLogger(args.output_prefix + 'validation.csv')
validator.register_observer(v_logger)
validator.register_observer(v_printer)
trainer.register_observer(validator)

trainer.train()
model_filename = args.output_prefix + 'final.pt'
torch.save(unet, model_filename)
