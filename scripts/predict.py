#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--images', nargs='+', required=True,
                    help='The path to the image to predict')
parser.add_argument('-a', '--masks', nargs='+', default=[],
                    help='The path to the ROI mask')
parser.add_argument('-c', '--cropping-shape', nargs=3, type=int, required=True,
                    help='The shape of the bounding box to crop ROI')
parser.add_argument('-o', '--output-dir', required=True, 
                    help='The folder to save the prediction; the output will '
                         'be named as ${image_filename}_pred.nii.gz')
parser.add_argument('-m', '--model', required=True,
                    help='The path to the model')
parser.add_argument('-l', '--label-image', required=True,
                    help='The path to a label image for one-hot decoding')
args = parser.parse_args()

import os
import torch
import nibabel as nib
import numpy as np
from image_processing_3d import crop3d, uncrop3d, resize_bbox3d, calc_bbox3d

model = torch.load(args.model)
labels = np.unique(nib.load(args.label_image).get_data())

if not os.path.isdir(args.output_dir):
    os.makedirs(args.output_dir)

if len(args.masks) == 0:
    args.masks = [None] * len(args.images)

for image_path, mask_path in zip(args.images, args.masks):
    obj = nib.load(image_path)
    image = obj.get_data()
    if mask_path:
        mask = nib.load(mask_path).get_data()
        bbox = calc_bbox3d(mask)
        extended_bbox = resize_bbox3d(bbox, args.cropping_shape)
        image, source_bbox, target_bbox = crop3d(image, extended_bbox)
    else:
        source_bbox = None
        target_bbox = None
    input = torch.from_numpy(image[None, None, ...]).float()
    if torch.cuda.is_available():
        input = input.cuda()
    pred = model(input).data
    if torch.cuda.is_available():
        pred = pred.cpu()
    pred = pred.numpy()[0, ...]

    if pred.shape[0] == 1: # binary prediction
        seg = np.squeeze(pred) > 0.5
    else:
        seg = np.argmax(pred, axis=0)
        seg = labels[seg]
    if source_bbox:
        seg = uncrop3d(seg, obj.shape, source_bbox, target_bbox)

    output_obj = nib.Nifti1Image(seg, obj.affine, obj.header)
    basename = os.path.basename(image_path).replace('.nii', '_seg.nii')
    output_obj.to_filename(os.path.join(args.output_dir, basename))
