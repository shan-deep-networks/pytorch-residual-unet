# -*- coding: utf-8 -*-

from torch.nn import Module
from pytorch_engine.blocks import PostActivConvBlock

from .blocks import ContractingBlock as CB
from .blocks import ExpandingBlock as EB
from .blocks import OutputBlock as OB


class ResidualUNet(Module):
    """Residual UNet

    """
    def __init__(self, in_channels, out_classes, num_contracting,
                 input_block_channels, max_channels=1024):
        """Initliaze  

        Args:
            in_channels (int): The number of channels of the input image
            out_classes (int): The number of output labels/classes
            input_block_channels (int): The number of output channels of the
                input channels, a.k.a the width of the network
            num_contracting (int): The number of the contracting blocks, a.k.a
                the depth of the network
            max_channels (int): The maximum number of channels of a conv layer

        """
        super().__init__()
        self.in_channels = in_channels
        self.out_classes = out_classes
        self.num_contracting = num_contracting
        self.input_block_channels = input_block_channels
        self.max_channels = max_channels

        self.cb0 = PostActivConvBlock(self.in_channels, input_block_channels)

        # encoding/contracting
        in_channels = input_block_channels
        out_channels = in_channels
        for i in range(self.num_contracting):
            setattr(self, 'cb%d'%(i+1), CB(in_channels,out_channels))
            in_channels = out_channels
            out_channels = self._calc_out_channels(in_channels)

        # decoding/expanding
        for i in range(self.num_contracting):
            cb = getattr(self, 'cb%d'%(self.num_contracting-i-1))
            out_channels = cb.out_channels
            setattr(self, 'eb%d'%(i+1), EB(in_channels,out_channels))
            in_channels = out_channels

        # multi-level feature output block
        in_channels = [getattr(self, 'eb%d'%(i+1)).out_channels
                       for i in range(self.num_contracting)]
        self.out = OB(in_channels, self.out_classes)
        

    def _calc_out_channels(self, in_channels):
        """Calculate the number of output channels/features

        Args:
            in_channels (int): The number of input channels/features

        Returns:
            out_channels (int): The number of output channels/features

        """
        out_channels = min(in_channels * 2, self.max_channels)
        return out_channels

    def forward(self, input):
        # encoding/contracting
        output = self.cb0(input)
        shortcuts = [output]
        for i in range(self.num_contracting):
            output = getattr(self, 'cb%d'%(i+1))(output)
            if i < self.num_contracting - 1:
                shortcuts.insert(0, output)

        # decoding/expanding
        outputs = list()
        for i, shortcut in enumerate(shortcuts):
            output = getattr(self, 'eb%d'%(i+1))(output, shortcut)
            outputs.append(output)
        
        # output
        output = self.out(outputs)

        return output
