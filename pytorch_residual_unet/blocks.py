# -*- coding: utf-8 -*-

import torch
from torch.nn import Module, Softmax, Sigmoid

from pytorch_engine.layers import ThreeConv, ProjConv, Activation, Upsample
from pytorch_engine.layers import Normalization
from pytorch_engine.layers import Dropout


class ContractingBlock(Module):
    """Residual UNet contracting block
    
    Attributes:
        in_channels (int): The number of input channels/features
        out_channels (int): The number of output channels/features

    """
    def __init__(self, in_channels, out_channels):
        """Initialize

        Args:
            in_channels (int): The number of input channels/features
            out_channels (int): The number of output channels/features

        """
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.conv1 = ThreeConv(self.in_channels, self.out_channels,
                               stride=2, bias=False)
        self.norm1 = Normalization(out_channels)
        self.activ1 = Activation()

        self.conv2 = ThreeConv(self.out_channels, self.out_channels,
                               stride=1, bias=False)
        self.norm2 = Normalization(out_channels)
        self.activ2 = Activation()

    def forward(self, input):
        identity = self.conv1(input)
        residue = self.norm1(identity)
        residue = self.activ1(residue)
        residue = self.conv2(residue)
        output = identity + residue
        output = self.norm2(output)
        output = self.activ2(output)
        return output


class ExpandingBlock(Module):
    """Residual UNet expanding block with long shortcut
    
    Attributes:
        in_channels (int): The number of input channels/features
        out_channels (int): The number of output channels/features

    """
    def __init__(self, in_channels, out_channels):
        """Initialize

        Args:
            in_channels (int): The number of input channels/features
            out_channels (int): The number of output channels/features

        """
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.conv1 = ProjConv(self.in_channels, self.out_channels,
                              stride=1, bias=False)
        self.norm1 = Normalization(self.out_channels)
        self.activ1 = Activation()
        self.up1 = Upsample(scale_factor=2)

        self.conv2 = ThreeConv(self.out_channels, self.out_channels,
                               stride=1, bias=False)
        self.norm2 = Normalization(self.out_channels)
        self.activ2 = Activation()

        self.dropout = Dropout()
        self.conv3 = ThreeConv(self.out_channels*2, self.out_channels,
                               stride=1, bias=False)
        self.norm3 = Normalization(self.out_channels)
        self.activ3 = Activation()

    def forward(self, input, shortcut):
        output = self.conv1(input)
        output = self.norm1(output)
        output = self.activ1(output)
        output = self.up1(output)
        output = self.conv2(output)
        output = self.norm2(output)
        output = self.activ2(output)
        output = torch.cat((output, shortcut), dim=1) # concat channels
        output = self.dropout(output)
        output = self.conv3(output)
        output = self.norm3(output)
        output = self.activ3(output)
        return output


class OutputBlock(Module):
    """Output block
    
    Attributes:
        in_channels (list): The number of input channels for all levels from
            bottom to top
        out_channels (int): The number of output classes/labels

    """
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.in_channels = in_channels
        if out_channels == 2:
            out_channels = 1
        self.out_channels = out_channels
        for i, num in enumerate(self.in_channels):
            conv = ProjConv(num, out_channels, stride=1, bias=True)
            setattr(self, 'conv%d'%(i+1), conv)
            setattr(self, 'up%d'%(i+1), Upsample(scale_factor=2))
        if self.out_channels == 1:
            self.out_activ = Sigmoid()
        else:
            self.out_activ = Softmax(dim=1)

    def forward(self, inputs):
        output = self.conv1(inputs[0])
        for i, input in enumerate(inputs[1:]):
            output = getattr(self, 'up%d'%(i+1))(output)
            output = getattr(self, 'conv%d'%(i+2))(input) + output
        output = self.out_activ(output)
        return output
