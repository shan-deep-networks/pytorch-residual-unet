#!/usr/bin/env python
# -*- coding: utf-8 -*-

import torch
from torchviz import make_dot

from pytorch_residual_unet.model import ResidualUNet as Net


input_block_channels = 16
num_contracting = 4
image_channels = 1
num_batches = 1
out_classes = 10
x, y, z = (128, 96, 96)

image = torch.randn(num_batches, image_channels, x, y, z)
print(image.shape)

net = Net(image_channels, out_classes, num_contracting, input_block_channels)
print(net)
output = net(image)

dot = make_dot(output, params=dict(net.named_parameters()))
dot.format = 'svg'
dot.render('residual_unet')
